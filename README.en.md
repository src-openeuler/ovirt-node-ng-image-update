# ovirt-node-ng-image-update

#### Description
This package will update an  oVirt Node Next host with the new image.

**Note**

The source code file is as follows:
* ovirt-node-ng-image-update-4.3.8.tar.gz
* ovirt-node-ng-image.squashfs.img
* ovirt-node-ng.spec
* product.img

The ovirt-node-ng-image.squashfs.img file cannot be uploaded to the gitee repository because it is larger than 300MB, please download [http://openkylin.com/repo/](http://openkylin.com/repo/ovirt/IMAGES/ovirt-node-ng-image.squashfs.img).
#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
