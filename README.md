# ovirt-node-ng-image-update

#### 介绍
该软件包将使用新镜像更新oVirt Node Next主机。

**特别说明**

源码文件如下：
* ovirt-node-ng-image-update-4.3.8.tar.gz
* ovirt-node-ng-image.squashfs.img
* ovirt-node-ng.spec
* product.img

其中ovirt-node-ng-image.squashfs.img文件因大于300MB无法上传至gitee仓库，请移步至[http://openkylin.com/repo/](http://openkylin.com/repo/ovirt/IMAGES/ovirt-node-ng-image.squashfs.img)下载。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
